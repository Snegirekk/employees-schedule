<?php

namespace App\Resource\Schedule;

use App\Resource\SingleResourceInterface;

class ScheduleDateResource implements SingleResourceInterface
{
    /**
     * @var \DateTimeInterface
     */
    private $day;

    /**
     * @var ScheduleTimeRangeResource[]
     */
    private $timeRanges;

    /**
     * @return \DateTimeInterface
     */
    public function getDay(): \DateTimeInterface
    {
        return $this->day;
    }

    /**
     * @param \DateTimeInterface $day
     * @return ScheduleDateResource
     */
    public function setDay(\DateTimeInterface $day): ScheduleDateResource
    {
        $this->day = $day;
        return $this;
    }

    /**
     * @return ScheduleTimeRangeResource[]
     */
    public function getTimeRanges(): array
    {
        return $this->timeRanges;
    }

    /**
     * @param ScheduleTimeRangeResource[] $timeRanges
     * @return ScheduleDateResource
     */
    public function setTimeRanges(array $timeRanges): ScheduleDateResource
    {
        $this->timeRanges = $timeRanges;
        return $this;
    }

    /**
     * @param ScheduleTimeRangeResource $timeRange
     * @return ScheduleDateResource
     */
    public function addTimeRange(ScheduleTimeRangeResource $timeRange): ScheduleDateResource
    {
        $this->timeRanges[] = $timeRange;
        return $this;
    }

    /**
     * @return string Resource type
     */
    public function getResourceType(): string
    {
        return 'date';
    }
}