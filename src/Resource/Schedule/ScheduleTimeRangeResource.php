<?php

namespace App\Resource\Schedule;

use App\Resource\SingleResourceInterface;

class ScheduleTimeRangeResource implements SingleResourceInterface
{
    /**
     * @var string
     */
    private $start;

    /**
     * @var string
     */
    private $end;

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * @param string $start
     * @return ScheduleTimeRangeResource
     */
    public function setStart(string $start): ScheduleTimeRangeResource
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return string
     */
    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @param string $end
     * @return ScheduleTimeRangeResource
     */
    public function setEnd(string $end): ScheduleTimeRangeResource
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @return string Resource type
     */
    public function getResourceType(): string
    {
        return 'timeRange';
    }
}