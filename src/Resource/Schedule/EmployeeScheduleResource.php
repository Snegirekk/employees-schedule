<?php

namespace App\Resource\Schedule;

use App\Resource\SingleResourceInterface;

class EmployeeScheduleResource implements SingleResourceInterface
{
    /**
     * @var ScheduleDateResource[]
     */
    private $schedule = [];

    /**
     * @return ScheduleDateResource[]
     */
    public function getSchedule(): array
    {
        return $this->schedule;
    }

    /**
     * @param ScheduleDateResource[] $schedule
     * @return EmployeeScheduleResource
     */
    public function setSchedule(array $schedule): EmployeeScheduleResource
    {
        $this->schedule = $schedule;
        return $this;
    }

    /**
     * @return string Resource type
     */
    public function getResourceType(): string
    {
        return 'schedule';
    }
}