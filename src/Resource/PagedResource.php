<?php

namespace App\Resource;

class PagedResource implements PagedResourceInterface
{
    /**
     * @var int
     */
    private $totalResults;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $itemsPerPage;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * @var string
     */
    private $itemsType;

    /**
     * @var array
     */
    private $items = [];

    /**
     * @return int
     */
    public function getTotalResults(): int
    {
        return $this->totalResults;
    }

    /**
     * @param int $totalResults
     * @return PagedResourceInterface
     */
    public function setTotalResults(int $totalResults): PagedResourceInterface
    {
        $this->totalResults = $totalResults;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return PagedResourceInterface
     */
    public function setPage(int $page): PagedResourceInterface
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     * @return PagedResourceInterface
     */
    public function setItemsPerPage(int $itemsPerPage): PagedResourceInterface
    {
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @param int $totalPages
     * @return PagedResourceInterface
     */
    public function setTotalPages(int $totalPages): PagedResourceInterface
    {
        $this->totalPages = $totalPages;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemsType(): string
    {
        return $this->itemsType;
    }

    /**
     * @param string $itemsType
     * @return PagedResource
     */
    public function setItemsType(string $itemsType): PagedResourceInterface
    {
        $this->itemsType = $itemsType;
        return $this;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return PagedResourceInterface
     */
    public function setItems(array $items): PagedResourceInterface
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @param SingleResourceInterface $item
     * @return PagedResourceInterface
     */
    public function addItem(SingleResourceInterface $item): PagedResourceInterface
    {
        $this->items[] = $item;
        return $this;
    }

    public function removeItem(SingleResourceInterface $item): PagedResourceInterface
    {
        unset($this->items[array_search($item, $this->items)]);
        $this->items = array_values($this->items);
        return $this;
    }

    /**
     * @return string
     */
    public function getResourceType(): string
    {
        return 'list';
    }
}