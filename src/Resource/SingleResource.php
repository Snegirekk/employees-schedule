<?php

namespace App\Resource;

class SingleResource implements SingleResourceInterface
{
    /**
     * @var string
     */
    protected $type ;

    /**
     * @var ResourceField[]
     */
    protected $fields = [];

    /**
     * SingleResource constructor.
     * @param string $resourceType
     */
    public function __construct(string $resourceType)
    {
        $this->type = $resourceType;
    }

    /**
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    public function get(string $field)
    {
        if (!$this->has($field)) {
            throw new \Exception(sprintf('The field "%s" doesn\'t exist', $field));
        }

        return $this->fields[$field]->getValue();
    }

    /**
     * @param string $field
     * @param mixed|null $value
     * @param null|string $serializedName
     * @return SingleResourceInterface
     * @throws \Exception
     */
    public function add(string $field, $value = null, ?string $serializedName = null): SingleResourceInterface
    {
        if (!$this->has($field)) {
            $type = $this->guessFieldType($value);
            $this->addWithType($field, $type, $value, $serializedName);
        }

        return $this;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function addScalarField(string $field, $value = null, ?string $serializedName = null): SingleResourceInterface
    {
        if ($value === null) {
            $type = ResourceField::RESOURCE_FIELD_TYPE_SCALAR;
        } else {
            $type = $this->guessFieldType($value);
        }

        if ($type !== ResourceField::RESOURCE_FIELD_TYPE_SCALAR) {
            throw new \Exception(sprintf('Expected scalar type, got "%s".', gettype($value)));
        }

        $this->addWithType($field, $type, $value, $serializedName);

        return $this;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function addIterableField(string $field, string $arrayType, $value = null, ?string $serializedName = null): SingleResourceInterface
    {
        if ($value === null) {
            $type = $arrayType;
        } else {
            $type = $this->guessFieldType($value);
        }

        if (
            $type !== ResourceField::RESOURCE_FIELD_TYPE_ARRAY &&
            $type !== ResourceField::RESOURCE_FIELD_TYPE_SCALAR_ARRAY &&
            $type !== ResourceField::RESOURCE_FIELD_TYPE_DATETIME_ARRAY &&
            $type !== ResourceField::RESOURCE_FIELD_TYPE_RESOURCE_ARRAY
        ) {
            throw new \Exception(sprintf('Expected array or \Traversable, got "%s".', gettype($value)));
        }

        $this->addWithType($field, $type, $value, $serializedName);

        return $this;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function addDateTimeField(string $field, $value = null, ?string $serializedName = null): SingleResourceInterface
    {
        if ($value === null) {
            $type = ResourceField::RESOURCE_FIELD_TYPE_DATETIME;
        } else {
            $type = $this->guessFieldType($value);
        }

        if ($type !== ResourceField::RESOURCE_FIELD_TYPE_DATETIME) {
            throw new \Exception(sprintf('Expected \DateTime, got "%s".', gettype($value)));
        }

        $this->addWithType($field, $type, $value, $serializedName);

        return $this;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function addResourceField(string $field, $value = null, ?string $serializedName = null): SingleResourceInterface
    {
        if ($value === null) {
            $type = ResourceField::RESOURCE_FIELD_TYPE_RESOURCE;
        } else {
            $type = $this->guessFieldType($value);
        }

        if ($type !== ResourceField::RESOURCE_FIELD_TYPE_RESOURCE) {
            throw new \Exception(sprintf('Expected \SingleResourceInterface, got "%s".', gettype($value)));
        }

        $this->addWithType($field, $type, $value, $serializedName);

        return $this;
    }

    /**
     * @param string[] $fields
     * @return SingleResourceInterface
     * @throws \Exception
     */
    public function addFields(array $fields = []): SingleResourceInterface
    {
        foreach ($fields as $field) {
            $this->add($field);
        }

        return $this;
    }

    /**
     * @param string $field
     * @param null $value
     * @return SingleResourceInterface
     * @throws \Exception
     */
    public function set(string $field, $value = null): SingleResourceInterface
    {
        if (!$this->has($field)) {
            throw new \Exception(sprintf('Can not set value for field "%s": the field doesn\'t exist.', $field));
        }

        $field = $this->fields[$field];
        $field
            ->setType($this->guessFieldType($value))
            ->setValue($value);

        return $this;
    }

    /**
     * @param string $field
     * @return SingleResourceInterface
     * @throws \Exception
     */
    public function remove(string $field): SingleResourceInterface
    {
        unset($this->fields[$field]);
        return $this;
    }

    /**
     * @param string $field
     * @return SingleResourceInterface
     */
    public function unset(string $field): SingleResourceInterface
    {
        if (!$this->has($field)) {
            return $this;
        }

        $this->fields[$field]->setValue(null);
        return $this;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function has(string $field): bool
    {
        return array_key_exists($field, $this->fields);
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @inheritDoc
     */
    public function getResourceType(): string
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if (preg_match('/(?P<accessor>[gs]et)(?P<field>[\w\d]+)/', $name, $match)) {
            $accessor = $match['accessor'];
            $field    = lcfirst($match['field']);
            unset($match);

            if (array_key_exists($field, $this->fields)) {
                switch ($accessor) {
                    case 'get':
                        return $this->get($field);
                    case 'set':
                        return $this->set($field, $arguments[0]);
                }
            }
        }

        return trigger_error(sprintf('Attempted to call an undefined method named "%s" of class "%s".', $name, __CLASS__), E_USER_ERROR);
    }

    /**
     * @param string $field
     * @param string $type
     * @param mixed|null $value
     * @param null|string $serializedName
     */
    private function addWithType(string $field, string $type, $value = null, ?string $serializedName = null): void
    {
        $this->fields[$field] = new ResourceField($field, $type, $value, $serializedName);
    }

    /**
     * @param mixed $value
     * @return string
     * @throws \Exception
     */
    private function guessFieldType(&$value): string
    {
        switch (true) {
            /** @noinspection PhpMissingBreakStatementInspection */
            case $value instanceof \Traversable:
                $value = iterator_to_array($value);
            /** @noinspection PhpMissingBreakStatementInspection */
            case is_array($value):
                if (count($value) === 0) {
                    return ResourceField::RESOURCE_FIELD_TYPE_ARRAY;
                } else {
                    $elementsType = $this->guessFieldType($value[array_key_first($value)]);

                    foreach ($value as $element) {
                        if ($elementsType !== $this->guessFieldType($element)) {
                            throw new \Exception('Unsupported resource field type.');
                        }
                    }

                    switch ($elementsType) {
                        case ResourceField::RESOURCE_FIELD_TYPE_ARRAY:
                            return ResourceField::RESOURCE_FIELD_TYPE_ARRAY;
                        case ResourceField::RESOURCE_FIELD_TYPE_SCALAR:
                            return ResourceField::RESOURCE_FIELD_TYPE_SCALAR_ARRAY;
                        case ResourceField::RESOURCE_FIELD_TYPE_DATETIME:
                            return ResourceField::RESOURCE_FIELD_TYPE_DATETIME_ARRAY;
                        case ResourceField::RESOURCE_FIELD_TYPE_RESOURCE:
                            return ResourceField::RESOURCE_FIELD_TYPE_RESOURCE_ARRAY;
                    }
                }
            case is_scalar($value) || is_null($value):
                return ResourceField::RESOURCE_FIELD_TYPE_SCALAR;
            case $value instanceof \DateTime:
                return ResourceField::RESOURCE_FIELD_TYPE_DATETIME;
            case $value instanceof SingleResourceInterface:
                return ResourceField::RESOURCE_FIELD_TYPE_RESOURCE;
            default:
                throw new \Exception('Unsupported resource field type.');
        }
    }
}