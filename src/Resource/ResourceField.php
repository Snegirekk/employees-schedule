<?php

namespace App\Resource;

class ResourceField
{
    const RESOURCE_FIELD_TYPE_SCALAR   = 'scalar';
    const RESOURCE_FIELD_TYPE_DATETIME = \DateTime::class;
    const RESOURCE_FIELD_TYPE_RESOURCE = SingleResourceInterface::class;

    const RESOURCE_FIELD_TYPE_ARRAY          = 'array';
    const RESOURCE_FIELD_TYPE_SCALAR_ARRAY   = 'scalar[]';
    const RESOURCE_FIELD_TYPE_DATETIME_ARRAY = \DateTime::class . '[]';
    const RESOURCE_FIELD_TYPE_RESOURCE_ARRAY = SingleResourceInterface::class . '[]';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var mixed|null
     */
    private $value;

    /**
     * @var string|null
     */
    private $serializedName;

    /**
     * ResourceField constructor.
     * @param string $name
     * @param string|null $type
     * @param mixed $value
     * @param string $serializedName
     */
    public function __construct(string $name, ?string $type = null, $value = null, ?string $serializedName = null)
    {
        $this->name           = $name;
        $this->type           = $type;
        $this->value          = $value;
        $this->serializedName = $serializedName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getSerializedName(): ?string
    {
        return $this->serializedName;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ResourceField
     */
    public function setType(string $type): ResourceField
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed|null $value
     * @return ResourceField
     */
    public function setValue($value): ResourceField
    {
        $this->value = $value;
        return $this;
    }

}