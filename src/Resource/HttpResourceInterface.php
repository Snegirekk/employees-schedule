<?php

namespace App\Resource;

use App\Resource\ApiMessage\MessageBagInterface;

interface HttpResourceInterface extends ResourceInterface
{
    const STATUS_SUCCESS  = 'success';
    const STATUS_ERROR    = 'error';
    const STATUS_REDIRECT = 'redirect';

    const HTTP_OK                   = 200;
    const HTTP_CREATED              = 201;
    const HTTP_NO_CONTENT           = 204;

    const HTTP_MULTIPLE_CHOICE      = 300;
    const HTTP_NOT_MODIFIED         = 304;

    const HTTP_BAD_REQUEST          = 400;
    const HTTP_UNAUTHORIZED         = 401;
    const HTTP_FORBIDDEN            = 403;
    const HTTP_NOT_FOUND            = 404;
    const HTTP_CONFLICT             = 409;
    const HTTP_UNPROCESSABLE_ENTITY = 422;

    const HTTP_SERVER_ERROR         = 500;

    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @param string $status
     * @return HttpResourceInterface
     */
    public function setStatus(string $status): HttpResourceInterface;

    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @param int $statusCode
     * @return HttpResourceInterface
     */
    public function setStatusCode(int $statusCode): HttpResourceInterface;

    /**
     * @return MessageBagInterface|null
     */
    public function getMessages(): ?MessageBagInterface;

    /**
     * @param MessageBagInterface|null $messages
     * @return HttpResourceInterface
     */
    public function setMessages(?MessageBagInterface $messages): HttpResourceInterface;

    /**
     * @return ResourceInterface|PagedResourceInterface|SingleResourceInterface|null
     */
    public function getResult(): ?ResourceInterface;

    /**
     * @param ResourceInterface|null $resource
     * @return HttpResourceInterface
     */
    public function setResult(?ResourceInterface $resource): HttpResourceInterface;

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @param string $key
     * @param string $value
     * @return HttpResourceInterface
     */
    public function addHeader(string $key, string $value): HttpResourceInterface;

    /**
     * @param string $key
     * @return HttpResourceInterface
     */
    public function removeHeader(string $key): HttpResourceInterface;
}