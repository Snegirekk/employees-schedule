<?php

namespace App\Resource\ApiMessage;

use Countable;
use Iterator;

interface MessageBagInterface extends Countable
{
    /**
     * @return Iterator
     */
    public function getMessages(): Iterator;

    /**
     * @param string $message
     * @param array  $context
     * @return MessageBagInterface
     * @TODO: create "Message" and "Context" interfaces and their normalizers
     */
    public function addMessage(string $message, array $context = []): MessageBagInterface;
}