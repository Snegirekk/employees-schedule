<?php

namespace App\Resource;

use App\Resource\ApiMessage\MessageBag;
use App\Resource\ApiMessage\MessageBagInterface;

class ErrorResource implements SingleResourceInterface
{
    /**
     * @var MessageBagInterface
     */
    private $errors;

    /**
     * ErrorResource constructor.
     * @param MessageBagInterface|null $messages
     */
    public function __construct(?MessageBagInterface $messages)
    {
        $this->errors = $messages ?? new MessageBag();
    }

    /**
     * @return MessageBagInterface
     */
    public function getErrors(): MessageBagInterface
    {
        return $this->errors;
    }

    /**
     * @param string $message
     * @param array $context
     * @return ErrorResource
     */
    public function addMessage(string $message, array $context): ErrorResource
    {
        $this->errors->addMessage($message, $context);
        return $this;
    }

    /**
     * @return string Resource type
     */
    public function getResourceType(): string
    {
        return 'error';
    }
}