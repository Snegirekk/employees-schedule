<?php

namespace App\Resource;

interface ResourceInterface
{
    /**
     * @return string Resource type
     */
    public function getResourceType(): string;
}