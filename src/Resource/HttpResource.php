<?php

namespace App\Resource;

use App\Resource\ApiMessage\MessageBag;
use App\Resource\ApiMessage\MessageBagInterface;

class HttpResource implements HttpResourceInterface
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var MessageBag
     */
    private $messages;

    /**
     * @var ResourceInterface|null
     */
    private $result;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * ResourceDecorator constructor.
     * @param string                            $status
     * @param int                               $statusCode
     * @param MessageBagInterface|null $messages
     * @param ResourceInterface|null            $result
     */
    public function __construct(
        string $status,
        int $statusCode,
        ?MessageBagInterface $messages = null,
        ?ResourceInterface $result = null
    ) {
        $this->status     = $status;
        $this->statusCode = $statusCode;
        $this->messages   = $messages;
        $this->result     = $result;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return MessageBagInterface|null
     */
    public function getMessages(): ?MessageBagInterface
    {
        return $this->messages;
    }

    /**
     * @return ResourceInterface|null
     */
    public function getResult(): ?ResourceInterface
    {
        return $this->result;
    }

    /**
     * @param string $status
     * @return HttpResource
     */
    public function setStatus(string $status): HttpResourceInterface
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param int $statusCode
     * @return HttpResource
     */
    public function setStatusCode(int $statusCode): HttpResourceInterface
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param MessageBagInterface $messages
     * @return HttpResource
     */
    public function setMessages(?MessageBagInterface $messages): HttpResourceInterface
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @param ResourceInterface|null $result
     * @return HttpResource
     */
    public function setResult(?ResourceInterface $result): HttpResourceInterface
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string $key
     * @param string $value
     * @return HttpResourceInterface
     */
    public function addHeader(string $key, string $value): HttpResourceInterface
    {
        $this->headers[$key] = $value;
        return $this;
    }

    public function removeHeader(string $key): HttpResourceInterface
    {
        unset($this->headers[$key]);
        return $this;
    }

    /**
     * @return string Resource type
     */
    public function getResourceType(): string
    {
        return $this->result->getResourceType();
    }
}