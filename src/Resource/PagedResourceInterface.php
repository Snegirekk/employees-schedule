<?php

namespace App\Resource;

interface PagedResourceInterface extends ResourceInterface
{
    /**
     * @return int
     */
    public function getTotalResults(): int;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @return int
     */
    public function getItemsPerPage(): int;

    /**
     * @return int
     */
    public function getTotalPages(): int;

    /**
     * @return string
     */
    public function getItemsType(): string;

    /**
     * @return SingleResourceInterface[]|array
     */
    public function getItems(): array;

    /**
     * @param int $totalResults
     * @return PagedResourceInterface
     */
    public function setTotalResults(int $totalResults): PagedResourceInterface;

    /**
     * @param int $page
     * @return PagedResourceInterface
     */
    public function setPage(int $page): PagedResourceInterface;

    /**
     * @param int $itemsPerPage
     * @return PagedResourceInterface
     */
    public function setItemsPerPage(int $itemsPerPage): PagedResourceInterface;

    /**
     * @param int $totalPages
     * @return PagedResourceInterface
     */
    public function setTotalPages(int $totalPages): PagedResourceInterface;

    /**
     * @param string $itemsType
     * @return PagedResourceInterface
     */
    public function setItemsType(string $itemsType): PagedResourceInterface;

    /**
     * @param array $items
     * @return PagedResourceInterface
     */
    public function setItems(array $items): PagedResourceInterface;

    /**
     * @param SingleResourceInterface $item
     * @return PagedResourceInterface
     */
    public function addItem(SingleResourceInterface $item): PagedResourceInterface;

    /**
     * @param SingleResourceInterface $item
     * @return PagedResourceInterface
     */
    public function removeItem(SingleResourceInterface $item): PagedResourceInterface;
}