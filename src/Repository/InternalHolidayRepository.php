<?php

namespace App\Repository;

use App\Entity\InternalHoliday;
use Doctrine\ORM\EntityRepository;

class InternalHolidayRepository extends EntityRepository
{
    /**
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return InternalHoliday[]
     */
    public function findByDates(\DateTimeInterface $startDate, \DateTimeInterface $endDate)
    {
        $qb  = $this->createQueryBuilder('ih');
        $qb
            ->andWhere('ih.startDate <= :endDate')
            ->andWhere('ih.endDate >= :startDate')

            ->setParameters([
                'startDate' => $startDate,
                'endDate'   => $endDate,
            ]);

        return $qb->getQuery()->getResult();
    }
}