<?php

namespace App\Repository;

use App\Entity\Employee;
use App\Entity\Vacation;
use Doctrine\ORM\EntityRepository;

class VacationRepository extends EntityRepository
{
    /**
     * @param Employee $employee
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return Vacation[]
     */
    public function findByEmployeeAndDates(Employee $employee, \DateTimeInterface $startDate, \DateTimeInterface $endDate)
    {
        $qb  = $this->createQueryBuilder('v');
        $qb
            ->andWhere('v.employee = :employee')
            ->andWhere('DATE(CONCAT(DATE_FORMAT(:endDate, \'%Y-\'), v.startMonth, \'-\', v.startDay)) <= DATE(:endDate)')
            ->andWhere('DATE(CONCAT(DATE_FORMAT(:startDate, \'%Y-\'), v.endMonth, \'-\', v.endDay)) >= DATE(:startDate)')

            ->setParameters([
                'employee'  => $employee,
                'startDate' => $startDate->format('Y-n-j'),
                'endDate'   => $endDate->format('Y-n-j'),
            ]);

        return $qb->getQuery()->getResult();
    }
}