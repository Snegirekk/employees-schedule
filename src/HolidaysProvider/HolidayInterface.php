<?php

namespace App\HolidaysProvider;

interface HolidayInterface
{
    /**
     * @return \DateTimeInterface
     */
    public function getStart(): \DateTimeInterface;

    /**
     * @return \DateTimeInterface
     */
    public function getEnd(): \DateTimeInterface;
}