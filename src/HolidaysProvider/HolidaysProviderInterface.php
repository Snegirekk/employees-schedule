<?php

namespace App\HolidaysProvider;

interface HolidaysProviderInterface
{
    /**
     * Provides an array of holiday representations.
     *
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return HolidayInterface[]
     */
    public function getHolidays(\DateTimeInterface $startDate, \DateTimeInterface $endDate): array;
}