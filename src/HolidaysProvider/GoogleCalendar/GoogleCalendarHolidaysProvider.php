<?php

namespace App\HolidaysProvider\GoogleCalendar;

use App\ApiClient\GoogleCalendar\GoogleCalendarClient;
use App\HolidaysProvider\Holiday;
use App\HolidaysProvider\HolidayInterface;
use App\HolidaysProvider\HolidaysProviderInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GoogleCalendarHolidaysProvider implements HolidaysProviderInterface
{
    /**
     * @var GoogleCalendarClient
     */
    private $googleCalendarClient;

    /**
     * GoogleCalendarHolidaysProvider constructor.
     * @param GoogleCalendarClient $googleCalendarClient
     */
    public function __construct(GoogleCalendarClient $googleCalendarClient)
    {
        $this->googleCalendarClient = $googleCalendarClient;
    }

    /**
     * Provides an array of holiday representations.
     *
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return HolidayInterface[]
     * @throws TransportExceptionInterface
     * @throws NotEncodableValueException
     */
    public function getHolidays(\DateTimeInterface $startDate, \DateTimeInterface $endDate): array
    {
        $eventsList = $this->googleCalendarClient->eventsList($startDate, $endDate);
        $holidays   = [];

        foreach ($eventsList->getItems() as $event) {
            $start = $event->getStart()->getDate();
            $end   = $event->getEnd()->getDate();

            $holidays[] = new Holiday($start, $end);
        }

        return $holidays;
    }
}