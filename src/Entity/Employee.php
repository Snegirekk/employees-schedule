<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="employees")
 */
class Employee
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", options={"unsigned": true})
     */
    private $id;

    /**
     * @var Schedule
     * @ORM\OneToOne(targetEntity="App\Entity\Schedule", inversedBy="employee", cascade={"Persist"})
     */
    private $schedule;

    /**
     * @var Vacation[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Vacation", mappedBy="employee", cascade={"Persist"})
     */
    private $vacations;

    /**
     * Employee constructor.
     */
    public function __construct()
    {
        $this->vacations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Schedule
     */
    public function getSchedule(): Schedule
    {
        return $this->schedule;
    }

    /**
     * @param Schedule $schedule
     * @return Employee
     */
    public function setSchedule(Schedule $schedule): Employee
    {
        $this->schedule = $schedule;
        return $this;
    }

    /**
     * @return Vacation[]|Collection
     */
    public function getVacations()
    {
        return $this->vacations;
    }

    /**
     * @param Vacation $vacation
     * @return Employee
     */
    public function addVacation(Vacation $vacation)
    {
        $this->vacations->add($vacation);
        $vacation->setEmployee($this);
        return $this;
    }

    /**
     * @param Vacation $vacation
     * @return Employee
     */
    public function removeVacation(Vacation $vacation)
    {
        $this->vacations->removeElement($vacation);
        return $this;
    }

}