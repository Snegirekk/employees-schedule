<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InternalHolidayRepository")
 * @ORM\Table(name="internal_holidays")
 */
class InternalHoliday
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", options={"unsigned": true})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return InternalHoliday
     */
    public function setTitle(string $title): InternalHoliday
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @param \DateTimeInterface $startDate
     * @return InternalHoliday
     */
    public function setStartDate(\DateTimeInterface $startDate): InternalHoliday
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @param \DateTimeInterface $endDate
     * @return InternalHoliday
     */
    public function setEndDate(\DateTimeInterface $endDate): InternalHoliday
    {
        $this->endDate = $endDate;
        return $this;
    }

}