<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VacationRepository")
 * @ORM\Table(name="vacations")
 */
class Vacation
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", options={"unsigned": true})
     */
    private $id;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="vacations")
     * @ORM\JoinColumn(onDelete="cascade")
     */
    private $employee;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 1})
     */
    private $startMonth;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 1})
     */
    private $startDay;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 2})
     */
    private $endMonth;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 1})
     */
    private $endDay;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     * @return Vacation
     */
    public function setEmployee(Employee $employee): Vacation
    {
        $this->employee = $employee;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartMonth(): int
    {
        return $this->startMonth;
    }

    /**
     * @param int $startMonth
     * @return Vacation
     */
    public function setStartMonth(int $startMonth): Vacation
    {
        $this->startMonth = $startMonth;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartDay(): int
    {
        return $this->startDay;
    }

    /**
     * @param int $startDay
     * @return Vacation
     */
    public function setStartDay(int $startDay): Vacation
    {
        $this->startDay = $startDay;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndMonth(): int
    {
        return $this->endMonth;
    }

    /**
     * @param int $endMonth
     * @return Vacation
     */
    public function setEndMonth(int $endMonth): Vacation
    {
        $this->endMonth = $endMonth;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndDay(): int
    {
        return $this->endDay;
    }

    /**
     * @param int $endDay
     * @return Vacation
     */
    public function setEndDay(int $endDay): Vacation
    {
        $this->endDay = $endDay;
        return $this;
    }

}