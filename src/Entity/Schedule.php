<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="schedules")
 */
class Schedule
{
    const DAY_OF_WEEK_SUNDAY    = 'Sun';
    const DAY_OF_WEEK_MONDAY    = 'Mon';
    const DAY_OF_WEEK_TUESDAY   = 'Tue';
    const DAY_OF_WEEK_WEDNESDAY = 'Wed';
    const DAY_OF_WEEK_THURSDAY  = 'Thu';
    const DAY_OF_WEEK_FRIDAY    = 'Fri';
    const DAY_OF_WEEK_SATURDAY  = 'Sat';

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", options={"unsigned": true})
     */
    private $id;

    /**
     * @var Employee
     * @ORM\OneToOne(targetEntity="App\Entity\Employee", orphanRemoval=true, mappedBy="schedule")
     */
    private $employee;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 9})
     */
    private $startHour;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 0})
     */
    private $startMinute;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 18})
     */
    private $endHour;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 0})
     */
    private $endMinute;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 13})
     */
    private $dinnerBreakStartHour;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 0})
     */
    private $dinnerBreakStartMinute;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 14})
     */
    private $dinnerBreakEndHour;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"unsigned": true, "default": 0})
     */
    private $dinnerBreakEndMinute;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    private $holidays = [];

    /**
     * @param int $startHour
     * @param int $startMinute
     * @param int $endHour
     * @param int $endMinute
     * @return Schedule
     */
    public function setWorkTime(int $startHour, int $startMinute, int $endHour, int $endMinute): Schedule
    {
        $this->startHour   = $startHour;
        $this->startMinute = $startMinute;
        $this->endHour     = $endHour;
        $this->endMinute   = $endMinute;

        return $this;
    }

    /**
     * @param int $startHour
     * @param int $startMinute
     * @param int $endHour
     * @param int $endMinute
     * @return Schedule
     */
    public function setDinnerBreakTime(int $startHour, int $startMinute, int $endHour, int $endMinute): Schedule
    {
        $this->dinnerBreakStartHour   = $startHour;
        $this->dinnerBreakStartMinute = $startMinute;
        $this->dinnerBreakEndHour     = $endHour;
        $this->dinnerBreakEndMinute   = $endMinute;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     * @return Schedule
     */
    public function setEmployee(Employee $employee): Schedule
    {
        $this->employee = $employee;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartHour(): int
    {
        return $this->startHour;
    }

    /**
     * @param int $startHour
     * @return Schedule
     */
    public function setStartHour(int $startHour): Schedule
    {
        $this->startHour = $startHour;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartMinute(): int
    {
        return $this->startMinute;
    }

    /**
     * @param int $startMinute
     * @return Schedule
     */
    public function setStartMinute(int $startMinute): Schedule
    {
        $this->startMinute = $startMinute;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndHour(): int
    {
        return $this->endHour;
    }

    /**
     * @param int $endHour
     * @return Schedule
     */
    public function setEndHour(int $endHour): Schedule
    {
        $this->endHour = $endHour;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndMinute(): int
    {
        return $this->endMinute;
    }

    /**
     * @param int $endMinute
     * @return Schedule
     */
    public function setEndMinute(int $endMinute): Schedule
    {
        $this->endMinute = $endMinute;
        return $this;
    }

    /**
     * @return int
     */
    public function getDinnerBreakStartHour(): int
    {
        return $this->dinnerBreakStartHour;
    }

    /**
     * @param int $dinnerBreakStartHour
     * @return Schedule
     */
    public function setDinnerBreakStartHour(int $dinnerBreakStartHour): Schedule
    {
        $this->dinnerBreakStartHour = $dinnerBreakStartHour;
        return $this;
    }

    /**
     * @return int
     */
    public function getDinnerBreakStartMinute(): int
    {
        return $this->dinnerBreakStartMinute;
    }

    /**
     * @param int $dinnerBreakStartMinute
     * @return Schedule
     */
    public function setDinnerBreakStartMinute(int $dinnerBreakStartMinute): Schedule
    {
        $this->dinnerBreakStartMinute = $dinnerBreakStartMinute;
        return $this;
    }

    /**
     * @return int
     */
    public function getDinnerBreakEndHour(): int
    {
        return $this->dinnerBreakEndHour;
    }

    /**
     * @param int $dinnerBreakEndHour
     * @return Schedule
     */
    public function setDinnerBreakEndHour(int $dinnerBreakEndHour): Schedule
    {
        $this->dinnerBreakEndHour = $dinnerBreakEndHour;
        return $this;
    }

    /**
     * @return int
     */
    public function getDinnerBreakEndMinute(): int
    {
        return $this->dinnerBreakEndMinute;
    }

    /**
     * @param int $dinnerBreakEndMinute
     * @return Schedule
     */
    public function setDinnerBreakEndMinute(int $dinnerBreakEndMinute): Schedule
    {
        $this->dinnerBreakEndMinute = $dinnerBreakEndMinute;
        return $this;
    }

    /**
     * @return array
     */
    public function getHolidays(): array
    {
        return $this->holidays;
    }

    /**
     * @param string $holiday
     * @return Schedule
     */
    public function addHoliday(string $holiday): Schedule
    {
        if (!in_array($holiday, $this->holidays)) {
            $this->holidays[] = $holiday;
        }

        return $this;
    }

    /**
     * @param string $holiday
     * @return Schedule
     */
    public function removeHoliday(string $holiday): Schedule
    {
        $index = array_search($holiday, $this->holidays);

        if (false !== $index) {
            unset($this->holidays[$index]);
        }

        return $this;
    }

}