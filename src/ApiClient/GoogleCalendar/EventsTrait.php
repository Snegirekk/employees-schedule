<?php

namespace App\ApiClient\GoogleCalendar;

use App\ApiClient\GoogleCalendar\DTO\Event\EventsList;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait EventsTrait
{
    /**
     * @param \DateTimeInterface|null $start
     * @param \DateTimeInterface|null $end
     * @return EventsList
     * @throws TransportExceptionInterface
     * @throws NotEncodableValueException
     */
    public function eventsList(?\DateTimeInterface $start = null, ?\DateTimeInterface $end = null)
    {
        $endpoint = sprintf('calendars/%s/events', $this->calendarId);
        $response = $this->doGet($endpoint, [
            'timeMin' => (null === $start) ? null : $start->format('c'),
            'timeMax' => (null === $end) ? null : $end->format('c')
        ]);

        return $this->serializer->deserialize($response->getContent(), EventsList::class, 'json');
    }
}