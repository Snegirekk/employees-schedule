<?php

namespace App\ApiClient\GoogleCalendar;

use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GoogleCalendarClient
{
    use EventsTrait;

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $calendarId;

    /**
     * GoogleCalendarClient constructor.
     * @param HttpClientInterface $httpClient
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     * @param string $baseUrl
     * @param string $apiKey
     * @param string $calendarId
     */
    public function __construct(
        HttpClientInterface $httpClient,
        SerializerInterface $serializer,
        LoggerInterface $logger,
        string $baseUrl,
        string $apiKey,
        string $calendarId
    ) {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
        $this->logger     = $logger;
        $this->baseUrl    = $baseUrl;
        $this->apiKey     = $apiKey;
        $this->calendarId = urlencode($calendarId);
    }

    /**
     * @param string $endpoint
     * @param array $query An array of $key => $value query parameters.
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    protected function doGet(string $endpoint, array $query = []): ResponseInterface
    {
        return $this->request('GET', $endpoint, ['query' => $query]);
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    private function request(string $method, string $endpoint, array $options): ResponseInterface
    {
        $options['base_uri'] = $this->baseUrl;
        $options['query']    = array_merge(['key' => $this->apiKey], $options['query']);

        return $this->httpClient->request($method, $endpoint, $options);
    }
}