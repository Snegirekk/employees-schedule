<?php

namespace App\ApiClient\GoogleCalendar\DTO\Event;

class Creator
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var bool
     */
    private $self;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Creator
     */
    public function setEmail(string $email): Creator
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return Creator
     */
    public function setDisplayName(string $displayName): Creator
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSelf(): bool
    {
        return $this->self;
    }

    /**
     * @param bool $self
     * @return Creator
     */
    public function setSelf(bool $self): Creator
    {
        $this->self = $self;
        return $this;
    }

}