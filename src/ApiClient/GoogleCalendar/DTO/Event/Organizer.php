<?php

namespace App\ApiClient\GoogleCalendar\DTO\Event;

class Organizer
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var bool
     */
    private $self;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Organizer
     */
    public function setEmail(string $email): Organizer
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return Organizer
     */
    public function setDisplayName(string $displayName): Organizer
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSelf(): bool
    {
        return $this->self;
    }

    /**
     * @param bool $self
     * @return Organizer
     */
    public function setSelf(bool $self): Organizer
    {
        $this->self = $self;
        return $this;
    }

}