<?php

namespace App\ApiClient\GoogleCalendar\DTO\Event;

class Event
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $kind;

    /**
     * @var string
     */
    private $etag;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $htmlLink;

    /**
     * @var \DateTimeInterface
     */
    private $created;

    /**
     * @var \DateTimeInterface
     */
    private $updated;

    /**
     * @var string
     */
    private $summary;

    /**
     * @var Creator
     */
    private $creator;

    /**
     * @var Organizer
     */
    private $organizer;

    /**
     * @var Point
     */
    private $start;

    /**
     * @var Point
     */
    private $end;

    /**
     * @var string
     */
    private $transparency;

    /**
     * @var string
     */
    private $visibility;

    /**
     * @var string
     */
    private $iCalUID;

    /**
     * @var int
     */
    private $sequence;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Event
     */
    public function setId(string $id): Event
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     * @return Event
     */
    public function setKind(string $kind): Event
    {
        $this->kind = $kind;
        return $this;
    }

    /**
     * @return string
     */
    public function getEtag(): string
    {
        return $this->etag;
    }

    /**
     * @param string $etag
     * @return Event
     */
    public function setEtag(string $etag): Event
    {
        $this->etag = $etag;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Event
     */
    public function setStatus(string $status): Event
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlLink(): string
    {
        return $this->htmlLink;
    }

    /**
     * @param string $htmlLink
     * @return Event
     */
    public function setHtmlLink(string $htmlLink): Event
    {
        $this->htmlLink = $htmlLink;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @param \DateTimeInterface $created
     * @return Event
     */
    public function setCreated(\DateTimeInterface $created): Event
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @param \DateTimeInterface $updated
     * @return Event
     */
    public function setUpdated(\DateTimeInterface $updated): Event
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return Event
     */
    public function setSummary(string $summary): Event
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return Creator
     */
    public function getCreator(): Creator
    {
        return $this->creator;
    }

    /**
     * @param Creator $creator
     * @return Event
     */
    public function setCreator(Creator $creator): Event
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return Organizer
     */
    public function getOrganizer(): Organizer
    {
        return $this->organizer;
    }

    /**
     * @param Organizer $organizer
     * @return Event
     */
    public function setOrganizer(Organizer $organizer): Event
    {
        $this->organizer = $organizer;
        return $this;
    }

    /**
     * @return Point
     */
    public function getStart(): Point
    {
        return $this->start;
    }

    /**
     * @param Point $start
     * @return Event
     */
    public function setStart(Point $start): Event
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return Point
     */
    public function getEnd(): Point
    {
        return $this->end;
    }

    /**
     * @param Point $end
     * @return Event
     */
    public function setEnd(Point $end): Event
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransparency(): string
    {
        return $this->transparency;
    }

    /**
     * @param string $transparency
     * @return Event
     */
    public function setTransparency(string $transparency): Event
    {
        $this->transparency = $transparency;
        return $this;
    }

    /**
     * @return string
     */
    public function getVisibility(): string
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     * @return Event
     */
    public function setVisibility(string $visibility): Event
    {
        $this->visibility = $visibility;
        return $this;
    }

    /**
     * @return string
     */
    public function getICalUID(): string
    {
        return $this->iCalUID;
    }

    /**
     * @param string $iCalUID
     * @return Event
     */
    public function setICalUID(string $iCalUID): Event
    {
        $this->iCalUID = $iCalUID;
        return $this;
    }

    /**
     * @return int
     */
    public function getSequence(): int
    {
        return $this->sequence;
    }

    /**
     * @param int $sequence
     * @return Event
     */
    public function setSequence(int $sequence): Event
    {
        $this->sequence = $sequence;
        return $this;
    }

}