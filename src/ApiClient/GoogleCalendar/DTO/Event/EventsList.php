<?php

namespace App\ApiClient\GoogleCalendar\DTO\Event;

class EventsList
{
    /**
     * @var string
     */
    private $kind;

    /**
     * @var string
     */
    private $etag;

    /**
     * @var string
     */
    private $summary;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTimeInterface
     */
    private $updated;

    /**
     * @var string
     */
    private $timeZone;

    /**
     * @var string
     */
    private $accessRole;

    /**
     * @var string
     */
    private $nextPageToken;

    /**
     * @var string
     */
    private $nextSyncToken;

    /**
     * @var Event[]
     */
    private $items = [];

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     * @return EventsList
     */
    public function setKind(string $kind): EventsList
    {
        $this->kind = $kind;
        return $this;
    }

    /**
     * @return string
     */
    public function getEtag(): string
    {
        return $this->etag;
    }

    /**
     * @param string $etag
     * @return EventsList
     */
    public function setEtag(string $etag): EventsList
    {
        $this->etag = $etag;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return EventsList
     */
    public function setSummary(string $summary): EventsList
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return EventsList
     */
    public function setDescription(string $description): EventsList
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @param \DateTimeInterface $updated
     * @return EventsList
     */
    public function setUpdated(\DateTimeInterface $updated): EventsList
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeZone(): string
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     * @return EventsList
     */
    public function setTimeZone(string $timeZone): EventsList
    {
        $this->timeZone = $timeZone;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessRole(): string
    {
        return $this->accessRole;
    }

    /**
     * @param string $accessRole
     * @return EventsList
     */
    public function setAccessRole(string $accessRole): EventsList
    {
        $this->accessRole = $accessRole;
        return $this;
    }

    /**
     * @return string
     */
    public function getNextPageToken(): string
    {
        return $this->nextPageToken;
    }

    /**
     * @param string $nextPageToken
     * @return EventsList
     */
    public function setNextPageToken(string $nextPageToken): EventsList
    {
        $this->nextPageToken = $nextPageToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getNextSyncToken(): string
    {
        return $this->nextSyncToken;
    }

    /**
     * @param string $nextSyncToken
     * @return EventsList
     */
    public function setNextSyncToken(string $nextSyncToken): EventsList
    {
        $this->nextSyncToken = $nextSyncToken;
        return $this;
    }

    /**
     * @return Event[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Event[] $items
     * @return EventsList
     */
    public function setItems(array $items): EventsList
    {
        $this->items = $items;
        return $this;
    }
}