<?php

namespace App\ApiClient\GoogleCalendar\DTO\Event;

class Point
{
    /**
     * @var \DateTimeInterface
     */
    private $date;

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return Point
     */
    public function setDate(\DateTimeInterface $date): Point
    {
        $this->date = $date;
        return $this;
    }

}