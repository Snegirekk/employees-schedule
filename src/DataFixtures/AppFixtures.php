<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use App\Entity\InternalHoliday;
use App\Entity\Schedule;
use App\Entity\Vacation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $corporateParty = new InternalHoliday();
        $corporateParty
            ->setTitle('New Year corporate party')
            ->setStartDate(new \DateTime('2018-01-10 15:00:00'))
            ->setEndDate(new \DateTime('2018-01-11 00:00:00'));

        $manager->persist($corporateParty);

        $vacation11 = new Vacation();
        $vacation11
            ->setStartMonth(1)
            ->setStartDay(11)
            ->setEndMonth(1)
            ->setEndDay(25);

        $vacation12 = new Vacation();
        $vacation12
            ->setStartMonth(2)
            ->setStartDay(1)
            ->setEndMonth(2)
            ->setEndDay(15);

        $vacation21 = new Vacation();
        $vacation21
            ->setStartMonth(2)
            ->setStartDay(1)
            ->setEndMonth(3)
            ->setEndDay(1);

        $schedule1 = new Schedule();
        $schedule1
            ->setWorkTime(10, 0, 19, 0)
            ->setDinnerBreakTime(13, 0, 14, 0)
            ->addHoliday(Schedule::DAY_OF_WEEK_SATURDAY)
            ->addHoliday(Schedule::DAY_OF_WEEK_SUNDAY);

        $schedule2 = new Schedule();
        $schedule2
            ->setWorkTime(9, 0, 18, 0)
            ->setDinnerBreakTime(12, 0, 13, 0)
            ->addHoliday(Schedule::DAY_OF_WEEK_SATURDAY)
            ->addHoliday(Schedule::DAY_OF_WEEK_SUNDAY);

        $employee1 = new Employee();
        $employee1
            ->setSchedule($schedule1)
            ->addVacation($vacation11)
            ->addVacation($vacation12);

        $manager->persist($employee1);

        $employee2 = new Employee();
        $employee2
            ->setSchedule($schedule2)
            ->addVacation($vacation21);

        $manager->persist($employee2);

        $manager->flush();
    }
}
