<?php

namespace App\Exception;

use App\Resource\ApiMessage\MessageBagInterface;

interface ApiExceptionInterface extends MessageBagInterface
{
    /**
     * @return int
     */
    public function getResponseStatusCode(): int;

    /**
     * @param int $code
     * @return ApiException
     */
    public function setResponseStatusCode(int $code): ApiException;

    /**
     * @param string $message
     * @param array $context
     * @return ApiExceptionInterface
     */
    public static function create(string $message, array $context = []): ApiExceptionInterface;
}