<?php

namespace App\Exception;

use App\Resource\HttpResourceInterface;

class InvalidEntityException extends ApiException
{
    protected $responseStatusCode = HttpResourceInterface::HTTP_UNPROCESSABLE_ENTITY;
}