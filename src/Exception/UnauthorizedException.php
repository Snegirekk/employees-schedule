<?php

namespace App\Exception;

use App\Resource\HttpResourceInterface;

class UnauthorizedException extends ApiException
{
    protected $responseStatusCode = HttpResourceInterface::HTTP_UNAUTHORIZED;
}