<?php

namespace App\Exception;

use App\Resource\HttpResourceInterface;

class PageNotFoundException extends ApiException
{
    protected $responseStatusCode = HttpResourceInterface::HTTP_NOT_FOUND;
}