<?php

namespace App\Exception;

use App\Resource\HttpResourceInterface;

class ResourceNotFoundException extends ApiException
{
    protected $responseStatusCode = HttpResourceInterface::HTTP_NOT_FOUND;
}