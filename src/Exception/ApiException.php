<?php

namespace App\Exception;

use App\Resource\ApiMessage\MessageBagInterface;
use App\Resource\HttpResourceInterface;

class ApiException extends \Exception implements ApiExceptionInterface
{
    /**
     * @var int
     */
    protected $responseStatusCode = HttpResourceInterface::HTTP_BAD_REQUEST;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @return int
     */
    public function getResponseStatusCode(): int
    {
        return $this->responseStatusCode;
    }

    /**
     * @param int $code
     * @return ApiException
     */
    public function setResponseStatusCode(int $code): ApiException
    {
        $this->responseStatusCode = $code;
        return $this;
    }

    /**
     * @return \Iterator
     */
    public function getMessages(): \Iterator
    {
        foreach ($this->messages as $message) {
            $title   = key($message);
            $context = $message[$title];
            yield $title => $context;
        }
    }

    /**
     * @param string $message
     * @param array $context
     * @return MessageBagInterface|ApiExceptionInterface
     */
    public function addMessage(string $message, array $context = []): MessageBagInterface
    {
        $this->messages[] = [$message => $context];
        return $this;
    }

    /**
     * Count elements of an object
     * @link https://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->messages);
    }

    /**
     * @param string $message
     * @param array $context
     * @return ApiExceptionInterface
     */
    public static function create(string $message, array $context = []): ApiExceptionInterface
    {
        $exception = new static($message);
        $exception->addMessage($message, $context);

        return $exception;
    }
}