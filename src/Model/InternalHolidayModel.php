<?php

namespace App\Model;

use App\Entity\InternalHoliday;
use App\Repository\InternalHolidayRepository;
use Doctrine\Common\Persistence\ObjectRepository;

class InternalHolidayModel extends AbstractModel
{
    /**
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return InternalHoliday[]
     */
    public function findByDates(\DateTimeInterface $startDate, \DateTimeInterface $endDate): array
    {
        return $this->getRepository()->findByDates($startDate, $endDate);
    }

    /**
     * @return InternalHolidayRepository
     */
    protected function getRepository(): ObjectRepository
    {
        return $this->em->getRepository(InternalHoliday::class);
    }
}