<?php

namespace App\Model;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class AbstractModel
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ParameterBagInterface
     */
    protected $parameters;

    /**
     * AbstractModel constructor.
     * @param EntityManagerInterface $em
     * @param ParameterBagInterface $parameters
     */
    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameters)
    {
        $this->em         = $em;
        $this->parameters = $parameters;
    }

    /**
     * @param object $entity
     */
    public function update($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param object $entity
     */
    public function delete($entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * @return ObjectRepository
     */
    abstract protected function getRepository(): ObjectRepository;
}