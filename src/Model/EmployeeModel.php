<?php

namespace App\Model;

use App\Entity\Employee;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityNotFoundException;

class EmployeeModel extends AbstractModel
{
    /**
     * @param int $id
     * @return object|Employee
     * @throws EntityNotFoundException
     */
    public function getEmployee(int $id): Employee
    {
        $employee = $this->getRepository()->find($id);

        if (null === $employee) {
            throw EntityNotFoundException::fromClassNameAndIdentifier(Employee::class, ['id' => $id]);
        }

        return $employee;
    }

    /**
     * @return ObjectRepository
     */
    protected function getRepository(): ObjectRepository
    {
        return $this->em->getRepository(Employee::class);
    }
}