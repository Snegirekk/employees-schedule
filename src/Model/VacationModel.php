<?php

namespace App\Model;

use App\Entity\Employee;
use App\Entity\Vacation;
use App\Repository\VacationRepository;
use Doctrine\Common\Persistence\ObjectRepository;

class VacationModel extends AbstractModel
{
    /**
     * @param Employee $employee
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return Vacation[]
     */
    public function findByEmployeeAndDates(Employee $employee, \DateTimeInterface $startDate, \DateTimeInterface $endDate): array
    {
        return $this->getRepository()->findByEmployeeAndDates($employee, $startDate, $endDate);
    }

    /**
     * @return VacationRepository
     */
    protected function getRepository(): ObjectRepository
    {
        return $this->em->getRepository(Vacation::class);
    }
}