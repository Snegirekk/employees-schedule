<?php

namespace App\EventListener;

use App\Exception\ApiExceptionInterface;
use App\Resource\ApiMessage\MessageBag;
use App\Resource\ErrorResource;
use App\Resource\HttpResourceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ExceptionListener
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $environment;

    /**
     * ExceptionListener constructor.
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     * @param string $environment
     */
    public function __construct(SerializerInterface $serializer, LoggerInterface $logger, string $environment)
    {
        $this->serializer  = $serializer;
        $this->logger      = $logger;
        $this->environment = $environment;
    }

    /**
     * @param ExceptionEvent $event
     * @throws \Exception
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception    = $event->getException();
        $extraHeaders = [];

        if ($exception instanceof ApiExceptionInterface) {
            $statusCode = $exception->getResponseStatusCode();
            $messages   = $exception;

            $this->logger->error(sprintf('API Error: "%s"', $exception->getMessage()), iterator_to_array($messages->getMessages()));
        } elseif ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
            $message    = strpos($exception->getMessage(), 'object not found by the @ParamConverter annotation') === false ? $exception->getMessage() : MessageBag::RESOURCE_NOT_FOUND;
            $messages   = new MessageBag();

            $messages->addMessage($message);

            $extraHeaders = $exception->getHeaders();

            $this->logger->warning(sprintf('API HTTP Exception: "%s"', $exception->getMessage()), $exception->getTrace());
        } else {
            if ($this->environment === 'prod') {
                $statusCode = HttpResourceInterface::HTTP_SERVER_ERROR;
                $messages   = new MessageBag();
                $messages->addMessage(MessageBag::INTERNAL_ERROR);
            } else {
                throw $exception;
            }

            $this->logger->critical(sprintf('API Critical: "%s"', $exception->getMessage()), $exception->getTrace());
        }

        $resource   = new ErrorResource($messages);
        $serialized = $this->serializer->serialize($resource, 'json');
        $response   = new JsonResponse($serialized, $statusCode, $extraHeaders, true);

        $event->setResponse($response);
    }
}