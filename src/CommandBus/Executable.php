<?php

namespace App\CommandBus;

interface Executable
{
    /**
     * Indicates what the command or the query is initiated via http protocol.
     *
     * @return bool
     */
    public function isHttp(): bool;
}