<?php

namespace App\CommandBus;

use App\Resource\ApiMessage\MessageBagInterface;
use App\Resource\HttpResource;
use App\Resource\HttpResourceInterface;
use App\Resource\ResourceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class BaseHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var int
     */
    protected $defaultPage;

    /**
     * @var int
     */
    protected $defaultItemsPerPage;

    /**
     * @var ParameterBagInterface
     */
    private $parameters;

    /**
     * GetCollectionQueryHandler constructor.
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param UrlGeneratorInterface $router
     * @param ParameterBagInterface $parameters
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, UrlGeneratorInterface $router, ParameterBagInterface $parameters)
    {
        $this->em                  = $em;
        $this->logger              = $logger;
        $this->router              = $router;
        $this->defaultPage         = $parameters->get('pagination.default_page');
        $this->defaultItemsPerPage = $parameters->get('pagination.default_items_per_page');
        $this->parameters          = $parameters;
    }

    /**
     * @param ResourceInterface $resource
     * @param int|null $responseCode
     * @param MessageBagInterface|null $messages
     * @return HttpResourceInterface
     */
    protected function createHttpResource(ResourceInterface $resource, ?int $responseCode = HttpResourceInterface::HTTP_OK, ?MessageBagInterface $messages = null): HttpResourceInterface
    {
        return new HttpResource(
            HttpResourceInterface::STATUS_SUCCESS,
            $responseCode ?? HttpResourceInterface::HTTP_OK,
            $messages,
            $resource
        );
    }

    /**
     * @param string $key
     * @return mixed
     */
    protected function getParameter(string $key)
    {
        return $this->parameters->get($key);
    }
}