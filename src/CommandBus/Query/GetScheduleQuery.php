<?php

namespace App\CommandBus\Query;

use App\CommandBus\BaseExecutable;
use App\Entity\Employee;

class GetScheduleQuery extends BaseExecutable
{
    /**
     * @var \DateTime|null
     */
    private $startDate;

    /**
     * @var \DateTime|null
     */
    private $endDate;

    /**
     * @var Employee|null
     */
    private $employee;

    /**
     * @var bool
     */
    private $isInverse = false;

    /**
     * @return \DateTime|null
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime|null $startDate
     * @return GetScheduleQuery
     */
    public function setStartDate(?\DateTime $startDate): GetScheduleQuery
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime|null $endDate
     * @return GetScheduleQuery
     */
    public function setEndDate(?\DateTime $endDate): GetScheduleQuery
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return Employee|null
     */
    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee|null $employee
     * @return GetScheduleQuery
     */
    public function setEmployee(?Employee $employee): GetScheduleQuery
    {
        $this->employee = $employee;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInverse(): bool
    {
        return $this->isInverse;
    }

    /**
     * @param bool $isInverse
     * @return GetScheduleQuery
     */
    public function setIsInverse(bool $isInverse): GetScheduleQuery
    {
        $this->isInverse = $isInverse;
        return $this;
    }

}