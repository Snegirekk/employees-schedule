<?php

namespace App\CommandBus\Query\Handler;

use App\CommandBus\BaseHandler;
use App\Exception\PageNotFoundException;
use App\Exception\ResourceNotFoundException;
use App\Resource\ApiMessage\MessageBag;
use App\Resource\PagedResource;
use App\Resource\PagedResourceInterface;
use App\Resource\HttpResourceInterface;

abstract class GetPagedResourceQueryHandler extends BaseHandler
{
    /**
     * @param int    $page
     * @param int    $itemsPerPage
     * @param int    $totalResults
     * @param string $itemsType
     * @return PagedResourceInterface
     * @throws ResourceNotFoundException
     * @throws PageNotFoundException
     */
    protected function createPagedResource(int $page, int $itemsPerPage, int $totalResults, string $itemsType): PagedResourceInterface
    {
        if ($totalResults === 0) {
            $exception = new ResourceNotFoundException(MessageBag::NOTHING_MATCHES);
            $exception
                ->setResponseStatusCode(HttpResourceInterface::HTTP_NOT_FOUND)
                ->addMessage(MessageBag::NOTHING_MATCHES, []);

            throw $exception;
        }

        $totalPages = ceil($totalResults / $itemsPerPage);

        if ($totalPages < $page) {
            $exception = new PageNotFoundException(MessageBag::PAGE_NOT_FOUND);
            $exception
                ->setResponseStatusCode(HttpResourceInterface::HTTP_NOT_FOUND)
                ->addMessage(MessageBag::PAGE_NOT_FOUND, [
                    'requested_page' => $page,
                    'total_pages'    => $totalPages,
                    'items_per_page' => $itemsPerPage,
                ]);

            throw $exception;
        }

        $collection = new PagedResource();
        $collection
            ->setPage($page)
            ->setItemsPerPage($itemsPerPage)
            ->setTotalResults($totalResults)
            ->setTotalPages($totalPages)
            ->setItemsType($itemsType);

        return $collection;
    }
}