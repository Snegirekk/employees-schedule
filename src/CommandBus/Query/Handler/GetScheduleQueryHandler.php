<?php

namespace App\CommandBus\Query\Handler;

use App\CommandBus\BaseHandler;
use App\CommandBus\Query\GetScheduleQuery;
use App\Entity\InternalHoliday;
use App\Entity\Schedule;
use App\Entity\Vacation;
use App\HolidaysProvider\HolidayInterface;
use App\HolidaysProvider\HolidaysProviderInterface;
use App\Model\InternalHolidayModel;
use App\Model\VacationModel;
use App\Resource\ApiMessage\MessageBag;
use App\Resource\HttpResourceInterface;
use App\Resource\ResourceInterface;
use App\Resource\Schedule\EmployeeScheduleResource;
use App\Resource\Schedule\ScheduleDateResource;
use App\Resource\Schedule\ScheduleTimeRangeResource;
use League\Period\Exception;
use League\Period\Period;
use League\Period\Sequence;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GetScheduleQueryHandler extends BaseHandler
{
    /**
     * @var VacationModel
     */
    private $vacationModel;

    /**
     * @var InternalHolidayModel
     */
    private $internalHolidayModel;

    /**
     * @var HolidaysProviderInterface
     */
    private $holidaysProvider;

    /**
     * @param GetScheduleQuery $query
     * @return ResourceInterface
     * @throws Exception
     */
    public function handle(GetScheduleQuery $query): ResourceInterface
    {
        $employee  = $query->getEmployee();
        $schedule  = $employee->getSchedule();
        $startDate = $query->getStartDate();
        $endDate   = $query->getEndDate();
        $endDate->modify('next day');

        $vacations        = $this->vacationModel->findByEmployeeAndDates($employee, $startDate, $endDate);
        $internalHolidays = $this->internalHolidayModel->findByDates($startDate, $endDate);

        $messages = new MessageBag();

        try {
            $holidays = $this->holidaysProvider->getHolidays($startDate, $endDate);
        } catch (TransportExceptionInterface | NotEncodableValueException $e) {
            $holidays = [];

            $this->logger->error($e->getMessage(), $e->getTrace());
            $messages->addMessage('googleCalendarApiCallError', [
                'exceptionMessage' => $e->getMessage(),
                'trace'            => $e->getTrace(),
            ]);
        }

        $vacationPeriods        = $this->createVacationPeriodsSequence($vacations, $startDate, $endDate);
        $internalHolidayPeriods = $this->createInternalHolidayPeriodsSequence($internalHolidays);
        $holidayPeriods         = $this->createHolidayPeriodsSequence($holidays);

        $requestedPeriod = new Period($startDate, $endDate);
        $daysSequence    = new Sequence();

        foreach ($requestedPeriod->split('1 day') as $day) {
            $daysSequence->push($day);
        }

        $workPeriods = $this->createWorkPeriodsSequence($daysSequence, $schedule);

        $resultedSequence = $workPeriods
            ->subtract($vacationPeriods)
            ->subtract($internalHolidayPeriods)
            ->subtract($holidayPeriods);

        if ($query->isInverse()) {
            $resultedSequence = $daysSequence->subtract($resultedSequence);
        }

        $resource = new EmployeeScheduleResource();

        $dateResources = [];

        /** @var Period $period */
        foreach ($resultedSequence as $period) {
            $startDate = $period->getStartDate();
            $endDate   = $period->getEndDate();

            if ($startDate->format('d') !== $endDate->format('d')) {
                $endDate = $endDate->modify('-1 second');
            }

            $timeRangeResource = new ScheduleTimeRangeResource();
            $timeRangeResource
                ->setStart($startDate->format('Hi'))
                ->setEnd($endDate->format('Hi'));

            $dateString = $period->getStartDate()->format('Y-m-d');

            $dateResource = $dateResources[$dateString] ?? new ScheduleDateResource();
            $dateResource
                ->setDay(new \DateTime($period->getStartDate()->format('c')))
                ->addTimeRange($timeRangeResource);

            $dateResources[$dateString] = $dateResource;
        }

        $resource->setSchedule(array_values($dateResources));

        return $query->isHttp() ? $this->createHttpResource($resource, HttpResourceInterface::HTTP_OK, $messages) : $resource;
    }

    /**
     * @required
     * @param VacationModel $vacationModel
     */
    public function setVacationModel(VacationModel $vacationModel): void
    {
        $this->vacationModel = $vacationModel;
    }

    /**
     * @required
     * @param InternalHolidayModel $internalHolidayModel
     */
    public function setInternalHolidayModel(InternalHolidayModel $internalHolidayModel): void
    {
        $this->internalHolidayModel = $internalHolidayModel;
    }

    /**
     * @required
     * @param HolidaysProviderInterface $holidaysProvider
     */
    public function setHolidaysProvider(HolidaysProviderInterface $holidaysProvider): void
    {
        $this->holidaysProvider = $holidaysProvider;
    }

    /**
     * @param Sequence $daysSequence
     * @param Schedule $schedule
     * @return Sequence
     * @throws Exception
     */
    private function createWorkPeriodsSequence(Sequence $daysSequence, Schedule $schedule): Sequence
    {
        $workPeriods = new Sequence();

        $startFirstDayPartTime  = sprintf('T%d:%d', $schedule->getStartHour(), $schedule->getStartMinute());
        $endFirstDayPartTime    = sprintf('T%d:%d', $schedule->getDinnerBreakStartHour(), $schedule->getDinnerBreakStartMinute());
        $startSecondDayPartTime = sprintf('T%d:%d', $schedule->getDinnerBreakEndHour(), $schedule->getDinnerBreakEndMinute());
        $endSecondDayPartTime   = sprintf('T%d:%d', $schedule->getEndHour(), $schedule->getEndMinute());

        /** @var Period $day */
        foreach ($daysSequence as $day) {
            if (in_array($day->getStartDate()->format('D'), $schedule->getHolidays())) {
                continue;
            }

            $currentDate = $day->getStartDate()->format('Y-m-d');

            $startFirstDayPart  = $currentDate . $startFirstDayPartTime;
            $endFirstDayPart    = $currentDate . $endFirstDayPartTime;
            $startSecondDayPart = $currentDate . $startSecondDayPartTime;
            $endSecondDayPart   = $currentDate . $endSecondDayPartTime;

            if ($endFirstDayPart === $startSecondDayPart) {
                $workPeriods->push(new Period($startFirstDayPart, $endSecondDayPart));
            } else {
                $workPeriods->push(
                    new Period($startFirstDayPart, $endFirstDayPart),
                    new Period($startSecondDayPart, $endSecondDayPart)
                );
            }
        }

        return $workPeriods;
    }

    /**
     * @param Vacation[] $vacations
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     * @return Sequence
     * @throws Exception
     */
    private function createVacationPeriodsSequence(array $vacations, \DateTimeInterface $startDate, \DateTimeInterface $endDate): Sequence
    {
        $vacationPeriods = new Sequence();

        foreach ($vacations as $vacation) {
            $start = new \DateTime(sprintf('%d-%d-%d', $startDate->format('Y'), $vacation->getStartMonth(), $vacation->getStartDay()));
            $end   = new \DateTime(sprintf('%d-%d-%d', $endDate->format('Y'), $vacation->getEndMonth(), $vacation->getEndDay()));
            $end->modify('next day');

            $vacationPeriods->push(new Period($start, $end));
        }

        return $vacationPeriods;
    }

    /**
     * @param InternalHoliday[] $internalHolidays
     * @return Sequence
     * @throws Exception
     */
    private function createInternalHolidayPeriodsSequence(array $internalHolidays): Sequence
    {
        $internalHolidayPeriods = new Sequence();

        foreach ($internalHolidays as $internalHoliday) {
            $start = $internalHoliday->getStartDate();
            $end   = $internalHoliday->getEndDate();

            $internalHolidayPeriods->push(new Period($start, $end));
        }

        return $internalHolidayPeriods;
    }

    /**
     * @param HolidayInterface[] $holidays
     * @return Sequence
     * @throws Exception
     */
    private function createHolidayPeriodsSequence(array $holidays): Sequence
    {
        $holidayPeriods = new Sequence();

        foreach ($holidays as $holiday) {
            $start = $holiday->getStart();
            $end   = $holiday->getEnd();

            $holidayPeriods->push(new Period($start, $end));
        }

        return $holidayPeriods;
    }
}