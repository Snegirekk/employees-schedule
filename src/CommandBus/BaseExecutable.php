<?php

namespace App\CommandBus;

abstract class BaseExecutable implements Executable
{
    /**
     * @var bool
     */
    protected $isHttp = true;

    /**
     * @param bool $isHttp
     * @return BaseExecutable
     */
    public function setIsHttp(bool $isHttp): BaseExecutable
    {
        $this->isHttp = $isHttp;
        return $this;
    }

    /**
     * Indicates what the command or the query is initiated via http protocol.
     *
     * @return bool
     */
    public function isHttp(): bool
    {
        return $this->isHttp;
    }
}