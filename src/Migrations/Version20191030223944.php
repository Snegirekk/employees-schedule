<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030223944 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vacations (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, employee_id BIGINT UNSIGNED DEFAULT NULL, start_month SMALLINT UNSIGNED DEFAULT 1 NOT NULL, start_day SMALLINT UNSIGNED DEFAULT 1 NOT NULL, end_month SMALLINT UNSIGNED DEFAULT 2 NOT NULL, end_day SMALLINT UNSIGNED DEFAULT 1 NOT NULL, INDEX IDX_3B8290678C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schedules (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, start_hour SMALLINT UNSIGNED DEFAULT 9 NOT NULL, start_minute SMALLINT UNSIGNED DEFAULT 0 NOT NULL, end_hour SMALLINT UNSIGNED DEFAULT 18 NOT NULL, end_minute SMALLINT UNSIGNED DEFAULT 0 NOT NULL, dinner_break_start_hour SMALLINT UNSIGNED DEFAULT 13 NOT NULL, dinner_break_start_minute SMALLINT UNSIGNED DEFAULT 0 NOT NULL, dinner_break_end_hour SMALLINT UNSIGNED DEFAULT 14 NOT NULL, dinner_break_end_minute SMALLINT UNSIGNED DEFAULT 0 NOT NULL, holidays LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE internal_holidays (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employees (id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, schedule_id BIGINT UNSIGNED DEFAULT NULL, UNIQUE INDEX UNIQ_BA82C300A40BC2D5 (schedule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vacations ADD CONSTRAINT FK_3B8290678C03F15C FOREIGN KEY (employee_id) REFERENCES employees (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C300A40BC2D5 FOREIGN KEY (schedule_id) REFERENCES schedules (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY FK_BA82C300A40BC2D5');
        $this->addSql('ALTER TABLE vacations DROP FOREIGN KEY FK_3B8290678C03F15C');
        $this->addSql('DROP TABLE vacations');
        $this->addSql('DROP TABLE schedules');
        $this->addSql('DROP TABLE internal_holidays');
        $this->addSql('DROP TABLE employees');
    }
}
