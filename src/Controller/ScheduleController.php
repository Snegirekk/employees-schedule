<?php

namespace App\Controller;

use App\CommandBus\Query\GetScheduleQuery;
use App\Exception\BadRequestException;
use App\Exception\ResourceNotFoundException;
use App\Model\EmployeeModel;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ScheduleController extends ApiController
{
    /**
     * @param Request $request
     * @param EmployeeModel $employeeModel
     * @return Response
     * @throws \Throwable
     */
    public function getEmployeeSchedule(Request $request, EmployeeModel $employeeModel): Response
    {
        $getScheduleQuery = $this->createGetScheduleQuery($request, $employeeModel, false);
        return $this->exec($getScheduleQuery);
    }

    /**
     * @param Request $request
     * @param EmployeeModel $employeeModel
     * @return Response
     * @throws \Throwable
     */
    public function getEmployeeScheduleInverse(Request $request, EmployeeModel $employeeModel): Response
    {
        $getScheduleQuery = $this->createGetScheduleQuery($request, $employeeModel, true);
        return $this->exec($getScheduleQuery);
    }

    /**
     * @param Request $request
     * @param EmployeeModel $employeeModel
     * @param bool $isInverse
     * @return GetScheduleQuery
     */
    private function createGetScheduleQuery(Request $request, EmployeeModel $employeeModel, bool $isInverse): GetScheduleQuery
    {
        $startDateString = $request->query->get('startDate');
        $endDateString   = $request->query->get('endDate');
        $employeeId      = $request->query->getInt('userId');

        $startDate = $this->convertStringToDateTime($startDateString);
        $endDate   = $this->convertStringToDateTime($endDateString);

        try {
            $employee = $employeeModel->getEmployee($employeeId);
        } catch (EntityNotFoundException $e) {
            throw ResourceNotFoundException::create('Employee not found.', [
                'employeeId' => $employeeId,
            ]);
        }

        $query = new GetScheduleQuery();
        $query
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setEmployee($employee)
            ->setIsInverse($isInverse);

        return $query;
    }

    /**
     * @param null|string $string
     * @return \DateTime|null
     */
    private function convertStringToDateTime(?string $string): ?\DateTime
    {
        try {
            return $string ? new \DateTime($string) : null;
        } catch (\Exception $e) {
            throw BadRequestException::create('Invalid date format.', [
                'allowedFormat' => self::DATE_FORMAT,
                'actualValue'   => $string,
            ]);
        }
    }
}