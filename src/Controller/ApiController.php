<?php

namespace App\Controller;

use App\CommandBus\Executable;
use App\Exception\BadRequestException;
use App\Resource\ApiMessage\MessageBag;
use App\Resource\HttpResourceInterface;
use League\Tactician\Bundle\Middleware\InvalidCommandException;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;

abstract class ApiController extends AbstractController
{
    const DATE_FORMAT = 'Y-m-d';

    /**
     * @var CommandBus
     */
    protected $commandBus;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * ApiController constructor.
     * @param CommandBus $commandBus
     * @param SerializerInterface $serializer
     */
    public function __construct(CommandBus $commandBus, SerializerInterface $serializer)
    {
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
    }

    /**
     * @param Executable $commandOrQuery
     * @return Response
     * @throws \Throwable
     */
    protected function exec(Executable $commandOrQuery): Response
    {
        try {
            /** @var HttpResourceInterface $resource */
            $resource = $this->commandBus->handle($commandOrQuery);
        } catch (InvalidCommandException $e) {
            $violations = $e->getViolations();
            $exception  = new BadRequestException($e->getMessage());
            $context    = [];

            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $context[$violation->getPropertyPath()] = [
                    'reason' => $violation->getMessage(),
                    'value'  => $violation->getInvalidValue()
                ];
            }

            $exception->addMessage(MessageBag::CONSTRAINTS_FAILED, $context);
            throw $exception;
        }

        if ($resource instanceof \Generator) {
            $resource = $resource->getReturn();
        }

        return $this->createJsonResponse($resource);
    }

    /**
     * @param HttpResourceInterface $resource
     * @return JsonResponse
     */
    protected function createJsonResponse(HttpResourceInterface $resource): JsonResponse
    {
        $serialized = $this->serializer->serialize($resource, 'json', ['datetime_format' => self::DATE_FORMAT]);

        return new JsonResponse(
            $serialized,
            $resource->getStatusCode(),
            $resource->getHeaders(),
            true
        );
    }
}