<?php

namespace App\Serializer\Normalizer;

use App\Resource\HttpResourceInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ResourceDecoratorNormalizer extends ObjectNormalizer
{
    /**
     * @param $object
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $normalized = parent::normalize($object, $format, $context);
        return $normalized['result'];
    }

    /**
     * @param $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof HttpResourceInterface;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return false;
    }

}