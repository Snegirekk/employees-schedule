<?php

namespace App\Serializer\Normalizer;

use App\Resource\ApiMessage\MessageBagInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class MessageBagNormalizer implements NormalizerInterface
{
    /**
     * @inheritdoc
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $normalized = [];

        /** @var MessageBagInterface $object */
        foreach ($object->getMessages() as $message => $context) {
            foreach ($context as $key => $value) {
                $context[$key] = $value;
            }

            $normalized[] = [
                'message' => $message,
                'context' => empty($context) ? new \stdClass() : $context,
            ];
        }

        foreach ($normalized as $key => $value) {
            if ($value === null) {
                $normalized[$key] = new \stdClass();
            }
        }

        return $normalized;
    }

    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof MessageBagInterface;
    }
}