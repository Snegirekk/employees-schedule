<?php

namespace App\Serializer\Normalizer;

use App\Resource\ResourceInterface;
use App\Resource\SingleResourceInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ResourceNormalizer extends ObjectNormalizer implements NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    /**
     * @param ResourceInterface $object
     * @param string $format
     * @param array $context
     * @return array|bool|float|int|string
     * @throws ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $normalized = parent::normalize($object, $format, $context);
        unset($normalized['resourceType']);

        return $normalized;
    }

    /**
     * @param $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof SingleResourceInterface;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return false;
    }
}