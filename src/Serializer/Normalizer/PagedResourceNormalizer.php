<?php

namespace App\Serializer\Normalizer;

use App\Resource\PagedResourceInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PagedResourceNormalizer extends ObjectNormalizer
{
    /**
     * @param $object
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $normalized = parent::normalize($object, $format, $context);
        unset($normalized['resource_type']);

        return $normalized;
    }

    /**
     * @param $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof PagedResourceInterface;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        try {
            $reflection = new \ReflectionClass($type);
        } catch (\ReflectionException $exception) {
            return false;
        }

        return $reflection->implementsInterface(PagedResourceInterface::class);
    }

}