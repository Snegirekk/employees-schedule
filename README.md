### Запуск приложения

- Клонировать репозиторий
```
$ git clone git@bitbucket.org:Snegirekk/employees-schedule.git
$ cd employees-schedule
```

- Установить зависимости
```
$ composer install
```

- Настроить переменные окружения
```
$ cp .env .env.local
$ nano .env.local
```
Тут настроить креденшлсы для БД и для Google Calendar API
(`DATABASE_URL`, `GOOGLE_CALENDAR_BASE_URL`, `GOOGLE_CALENDAR_API_KEY`, `GOOGLE_CALENDAR_ID`)

- Создать БД, создать таблички, загрузить тестовые данные
```
$ bin/console doctrine:database:create
$ bin/console doctrine:migrations:migrate
$ bin/console doctrine:fixtures:load
```

- Запустить дев-сервер
```
$ bin/console server:start
```

- Слать запросы на `http://localhost:8000`

**Ендпоинты:**

`GET http://localhost:8000/schedule?startDate=Y-m-d&endDate=Y-m-d&userId=int`  
`GET http://localhost:8000/schedule/inverse?startDate=Y-m-d&endDate=Y-m-d&userId=int`